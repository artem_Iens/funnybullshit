package com.amdev.funnybullshit.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.amdev.funnybullshit.items.GridItem;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter{
    private ArrayList<String> urls;
    private Context context;

    public GridAdapter(Context context, ArrayList<String> urls) {
        this.context = context;
        this.urls = urls;
    }

    public void setData(ArrayList<String> urls) {
        this.urls = urls;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public Object getItem(int position) {
        return urls.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridItem gridItem = (GridItem) convertView;
        if(gridItem == null)
            gridItem = new GridItem(context);
        gridItem.loadImage(urls.get(position));
        return gridItem;
    }
}
