package com.amdev.funnybullshit.items;

import android.content.Context;
import android.util.AttributeSet;

import com.etsy.android.grid.util.DynamicHeightImageView;

public class GridItem extends DynamicHeightImageView {

    public GridItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridItem(Context context) {
        super(context);
    }

    public void loadImage(String url) {

    }
}
