package com.amdev.funnybullshit.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.amdev.funnybullshit.R;
import com.amdev.funnybullshit.adapters.GridAdapter;
import com.amdev.funnybullshit.helpers.BullshitHelper;
import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity {
    public static final String KEY_SAVED_DATA = "SAVED_DATA";
    public static final String SERVER = "http://www.placecage.com";
    public static final String GRAY = "/g";
    public static final String CRAZY = "/c";
    public static final int COUNT_OF_IMAGES = 50;
    public static final int MIN_IMG_SIZE_PX = 100;
    public static final int MAX_IMG_SIZE_PX = 1024;
    private StaggeredGridView gridView;
    private GridAdapter adapter;
    private Button btnReset;
    private ArrayList<String> urls;
    private Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        init(savedInstanceState);
        setListeners();
        showImages();
    }

    private void init(Bundle savedInstanceState) {
        gridView = (StaggeredGridView) findViewById(R.id.grid_view);
        btnReset = (Button) findViewById(R.id.btnReset);
        random = new Random();
        if(savedInstanceState != null) {
            urls = savedInstanceState.getStringArrayList(KEY_SAVED_DATA);
        } else {
            initUrls();
        }
    }

    private void setListeners() {
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initUrls();
                showImages();
            }
        });
    }

    /** Don't forget init urls before call this method*/
    private void showImages() {
        if(urls == null) return;
        if(adapter == null) {
            adapter = new GridAdapter(this, urls);
            gridView.setAdapter(adapter);
        } else {
            adapter.setData(urls);
        }
    }

    private void initUrls() {
        if(BullshitHelper.isNetworkConnected(this)) {
            generateRandomUrls();
        } else if(urls == null){
//            restoreLastUrls();
        }
    }

    private void generateRandomUrls() {
        urls = new ArrayList<String>();
        for(int i = 0; i < COUNT_OF_IMAGES; i++) {
            urls.add(getRandomUrl());
        }
    }

    private String getRandomUrl() {
        int height = getRandomSize();
        int width = getRandomSize();
        return SERVER + getRandomTypeOfCageFace() + "/" + width + "/" + height;
    }

    private int getRandomSize() {
        return random.nextInt(MAX_IMG_SIZE_PX + 1 - MIN_IMG_SIZE_PX) + MIN_IMG_SIZE_PX;
    }

    private String getRandomTypeOfCageFace() {
        switch (random.nextInt(3)) {
            case 0:
                return GRAY;
            case 1:
                return CRAZY;
            default:
                return "";
        }
    }

//    private ArrayList<String> restoreLastUrls() {
//
//    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(KEY_SAVED_DATA, urls);
    }
}
